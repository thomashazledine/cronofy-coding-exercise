# Cronofy Coding Exercise

## Overview

This is a "secret" message board. You can navigate to the message-board page, but you can only view it if you are registered and logged in. 

The project was built with React and initialised using the [`create-react-app` CLI tool](https://github.com/facebook/create-react-app). You can setup the project on your local machine by running `npm install`, and the app can be started using the command `npm run start`. This fires up a local server and allows you to view the project at [localhost:3000](http://localhost:3000/). It also watches for changes and live-updates the page when code changes have been made.

I've endeavoured to keep this project "serverless", so have used [Firebase](https://firebase.google.com/) for authentication and a realtime database (to keep track of registered users and messages posted on the board).

All the code I have written can be found in the `/src` directory. JSX components live in `/src/components`, styles live in `/src/scss` and raw SVG icons can be found in `/src/icons`.

## Notes

The exercise asked for a registration page. Because I've used social authentication (choose from Twitter, GitHub, or Facebook), the registration form is handled in a popup by whichever service you've chosen. To show the skills I imagine the task was looking for (form construction, handling submissions, saving to a DB), I've added the message board feature for logged-in users.

## Features of note

* Username and avatar in header for logged-in users.
* All icons provided with a dynamically-generated [inline SVG icon sprite](https://tomhazledine.com/inline-svg-icon-sprites/)
* Custom Webpack config for SVGs and styles: `/config/webpack.config.dev.js`
* Loading state (with spining Cronofy logo).
* Uses websockets to live-update evertime remote data changes (multiple users can be online at once, and will see new messages without having to refresh the page)

## Tests

Run tests with `npm run test` command. Due to time restraints, only a couple of tests have been writed (as examples to demonstrate how testing would be handled in a full project):

* _App_ - tests whether the main App component has been rendered.
* _Loader_ - tests whether the Loader component has been rendered.
* _Header_ - tests whether the Header component displays correct user information. It should show the name and avater of a user when available, and not loading the `.header__user` element at all when there is no user info available.

## Summary of commits

* create-react-app starter files
* ejecting from creat-react-app to allow sass and sourcemaps in webpack config
* adding scss support to build config
* adding support for svg icon sprite
* basic routing and components
* adding firebase config
* basic message-list component
* syncing messages to firebase db
* authentication with Firebase oAuth
* adding loading state
* breaking out router and authentication into separate files
* integrating user details into UI
* header logo and basic layout
* basic button styles
* message styles
* page styles
* misc theme styles
* loading animation
* tests for Header and Loader
