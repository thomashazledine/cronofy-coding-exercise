import React from 'react';

const Message = ({ message }) => (
    <li className="message">
        <p className="message__text">{message.message}</p>
        <span className="message__user">{message.user}</span>
    </li>
);

export default Message;
