import React from 'react';
import firebase from 'firebase';
import base, { firebaseApp } from '../base';

import Router from './Router';

class App extends React.Component {
    state = {
        user: false,
        loggedin: false,
        loading: true
    };

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.authHandler({ user });
            } else {
                this.setState({ loading: false });
            }
        });
    }

    authenticate = provider => {
        const authProvider = new firebase.auth[`${provider}AuthProvider`]();
        firebaseApp
            .auth()
            .signInWithPopup(authProvider)
            .then(this.authHandler);
    };

    authHandler = async authData => {
        const userId = authData.user.uid;

        const store = await base.fetch(`users`, { context: this });

        let user = false;

        if (!store[userId]) {
            user = {
                avatar: authData.user.photoURL,
                name: authData.user.displayName
            };
            await base.post(`users/${userId}`, {
                data: user
            });
        } else {
            user = store[userId];
        }

        this.setState({ user: user, loggedin: true, loading: false });
    };

    logout = async () => {
        await firebaseApp.auth().signOut();
        this.setState({ user: false, loggedin: false });
    };

    render() {
        return (
            <Router
                user={this.state.user}
                loggedin={this.state.loggedin}
                authenticate={this.authenticate}
                logout={this.logout}
                loading={this.state.loading}
            />
        );
    }
}

export default App;
