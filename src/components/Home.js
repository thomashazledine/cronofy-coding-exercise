import React from 'react';
import Header from './Header';
import Login from './Login';
import Loader from './Loader';

const Home = props => (
    <div>
        <Header user={props.user} loggedin={props.loggedin} logout={props.logout} />
        {props.loading ? (
            <Loader />
        ) : (
            <div>
                <div className="page__content">
                    <h1>Home</h1>
                    <p>
                        This site has a "secret" message board. You'll need to be logged-in to see
                        it. You can log in with whichever social account you prefer: GitHub,
                        Twitter, or Facebook
                    </p>
                    <button className="button" onClick={() => props.history.push('/secret')}>
                        See the secret
                    </button>
                </div>

                <Login
                    loggedin={props.loggedin}
                    authenticate={props.authenticate}
                    logout={props.logout}
                />
            </div>
        )}
    </div>
);

export default Home;
