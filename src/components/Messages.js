import React from 'react';
import base from '../base';
import AddMessageForm from './AddMessageForm';
import Message from './Message';

class Messages extends React.Component {
    constructor() {
        super();
        this.addMessage = this.addMessage.bind(this);
    }

    state = {
        messages: []
    };

    componentDidMount() {
        this.ref = base.syncState(`messages`, {
            context: this,
            state: 'messages',
            asArray: true
        });
    }

    addMessage(messageText) {
        const newMessage = {
            message: messageText,
            user: this.props.user.name
        };
        this.setState({
            messages: this.state.messages.concat([newMessage]) //updates Firebase and the local state
        });
    }

    componentWillUnmount() {
        base.removeBinding(this.ref);
    }

    render() {
        const messages = this.state.messages.map((message, key) => (
            <Message key={key} message={message} />
        ));
        return (
            <div className="messages">
                <h1 className="messages__title">Messages</h1>
                <ul className="messages__list">{messages}</ul>
                <AddMessageForm addMessage={this.addMessage} />
            </div>
        );
    }
}

export default Messages;
