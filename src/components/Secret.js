import React from 'react';
import Header from './Header';
import Messages from './Messages';
import Loader from './Loader';
import { Link } from 'react-router-dom';

const Secret = props => (
    <div>
        <Header user={props.user} loggedin={props.loggedin} logout={props.logout} />
        {props.loading ? (
            <Loader />
        ) : (
            <div>
                {props.loggedin ? (
                    <div>
                        <div className="page__content">
                            <p>
                                This page is a secret. You should only be able to see it when you're
                                logged-in. Leave a message to let others know you were here.
                            </p>
                        </div>
                        <Messages user={props.user} />
                    </div>
                ) : (
                    <div>
                        <h1>Woah, there!</h1>
                        <p>You need to be logged in to see this page</p>
                        <Link className="button" to="/">
                            Back to the homepage
                        </Link>
                    </div>
                )}
            </div>
        )}
    </div>
);

export default Secret;
