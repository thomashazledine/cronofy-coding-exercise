import React from 'react';
import { Link } from 'react-router-dom';

const Header = props => (
    <header className="header">
        <Link to="/" className="header__hero">
            <svg className="header__hero-logo">
                <use xlinkHref="#cronofy_logo" />
            </svg>
            <h3 className="header__hero-text">coding excercise</h3>
        </Link>
        {props.user ? (
            <div className="header__user">
                <div
                    className="user__avatar"
                    style={{ backgroundImage: `url('${props.user.avatar}')` }}
                />
                <span className="user__name">{props.user.name}</span>
                <button className="button header__logout logout" onClick={() => props.logout()}>
                    Log out
                </button>
            </div>
        ) : null}
    </header>
);

export default Header;
