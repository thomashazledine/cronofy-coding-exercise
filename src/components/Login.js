import React from 'react';

const Login = props => (
    <nav className="login">
        {!props.loggedin ? (
            <div>
                <p>You'll need to be logged-in to see the messages.</p>
                <button
                    className="button button--with-icon github"
                    onClick={() => props.authenticate('Github')}
                >
                    <svg className="button__icon">
                        <use xlinkHref="#github" />
                    </svg>
                    Log in with GitHub
                </button>
                <button
                    className="button button--with-icon twitter"
                    onClick={() => props.authenticate('Twitter')}
                >
                    <svg className="button__icon">
                        <use xlinkHref="#twitter" />
                    </svg>
                    Log in with Twitter
                </button>
                <button
                    className="button button--with-icon facebook"
                    onClick={() => props.authenticate('Facebook')}
                >
                    <svg className="button__icon">
                        <use xlinkHref="#facebook" />
                    </svg>
                    Log in with Facebook
                </button>
            </div>
        ) : (
            <button className="button logout" onClick={() => props.logout()}>
                Log out
            </button>
        )}
    </nav>
);

export default Login;
