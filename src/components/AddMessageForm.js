import React from 'react';

class AddMessageForm extends React.Component {
    constructor() {
        super();
        this.handleAddMessage = this.handleAddMessage.bind(this);
    }

    state = {
        message_valid: 'untested'
    };

    messageRef = React.createRef();
    nameRef = React.createRef();

    handleAddMessage(e) {
        e.preventDefault();
        const message = this.messageRef.current.value;
        if (message.length > 0) {
            this.props.addMessage(message);
            this.setState({
                message_valid: 'untested'
            });
            this.messageRef.current.value = '';
        } else {
            this.setState({
                message_valid: 'invalid'
            });
        }
    }

    render() {
        return (
            <form className="messages__form" onSubmit={this.handleAddMessage}>
                <svg className="messages__icon">
                    <use xlinkHref="#bubble" />
                </svg>
                <textarea
                    className={`messages__input ${this.state.message_valid}`}
                    ref={this.messageRef}
                    name="message_input"
                    type="text"
                    placeholder="Type your message here"
                />
                <button className="messages__button button">Add</button>
            </form>
        );
    }
}

export default AddMessageForm;
