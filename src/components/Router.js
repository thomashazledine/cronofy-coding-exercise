import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './Home';
import Secret from './Secret';
import _404 from './404';

class Router extends React.Component {
    render() {
        const HomeWithProps = props => {
            return (
                <Home
                    user={this.props.user}
                    loggedin={this.props.loggedin}
                    authenticate={this.props.authenticate}
                    logout={this.props.logout}
                    loading={this.props.loading}
                    {...props}
                />
            );
        };

        const SecretWithProps = props => {
            return (
                <Secret
                    user={this.props.user}
                    loggedin={this.props.loggedin}
                    logout={this.props.logout}
                    loading={this.props.loading}
                    {...props}
                />
            );
        };

        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={HomeWithProps} />
                    <Route path="/secret" component={SecretWithProps} />
                    <Route component={_404} />
                </Switch>
            </BrowserRouter>
        );
    }
}

export default Router;
