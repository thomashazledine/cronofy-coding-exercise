import React from 'react';
import Header from './Header';

const _404 = () => (
    <div>
        <Header />
        <h1>404 page not found</h1>
    </div>
);

export default _404;
