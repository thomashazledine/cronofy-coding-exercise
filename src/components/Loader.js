import React from 'react';

const Loader = props => (
    <div className="loader">
        <div className="loader__icon-wrapper">
        <svg className="loader__icon">
            <use xlinkHref="#cronofy_icon" />
        </svg>
        </div>
        <span className="loader__message">Loading...</span>
    </div>
);

export default Loader;
