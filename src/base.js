import Rebase from 're-base';
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
    apiKey: 'AIzaSyDWhW3i4oo5RS2UisRVPF7-rMe0gsptseE',
    authDomain: 'cronofy-excercise.firebaseapp.com',
    databaseURL: 'https://cronofy-excercise.firebaseio.com'
});

const base = Rebase.createClass(firebaseApp.database());

export { firebaseApp };
export default base;
