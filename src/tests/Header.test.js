import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from '../components/Header';

it('sets correct user data', () => {
    const headerWithUser = document.createElement('div');

    ReactDOM.render(
        <BrowserRouter>
            <Header
                user={{
                    avatar: 'https://path.to.avatar',
                    name: 'User #1'
                }}
                loggedin={true}
                logout={jest.fn()}
            />
        </BrowserRouter>,
        headerWithUser
    );

    expect(headerWithUser.querySelector('.user__name').innerHTML).toEqual('User #1');
    expect(headerWithUser.querySelector('.user__avatar').style.backgroundImage).toEqual(
        'url(https://path.to.avatar)'
    );
    expect(headerWithUser.querySelectorAll('.header__logout').length).toEqual(1);

    ReactDOM.unmountComponentAtNode(headerWithUser);
});

it('only loads user info when needed', () => {
    const headerWithoutUser = document.createElement('div');

    ReactDOM.render(
        <BrowserRouter>
            <Header user={false} loggedin={false} logout={jest.fn()} />
        </BrowserRouter>,
        headerWithoutUser
    );

    expect(headerWithoutUser.querySelectorAll('.header__user').length).toEqual(0);

    ReactDOM.unmountComponentAtNode(headerWithoutUser);
});
